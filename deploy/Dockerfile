# Copyright 2013 Thatcher Peskens
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

FROM ubuntu:14.04
ENV DEBIAN_FRONTEND noninteractive

ARG environment="staging"

# Install Nginx.
RUN \
 apt-get update && \
 apt-get install -y nginx && \
 rm -rf /var/lib/apt/lists/* && \
 echo "\ndaemon off;" >> /etc/nginx/nginx.conf && \
 chown -R www-data:www-data /var/lib/nginx

RUN echo "Built from env ${environment}"

RUN mkdir -p /srv/cert & mkdir -p /srv/www
COPY conf/${environment}/nginx.conf /etc/nginx/nginx.conf
ADD cert /srv/cert
ADD dist /srv/www
RUN chown -R www-data:www-data /srv/www && \
    chmod 755 -R /srv/www

# Define default command.
CMD ["nginx"]

# Expose ports.
EXPOSE 80
EXPOSE 443