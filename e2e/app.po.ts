import { browser, by, element } from 'protractor';

export class AppPage {
  navigateTo() {
    return browser.get('/');
  }

  openMenu(){
    return element(by.id('menuElement')).click()
  }
  openListItem(){
    element(by.id('SalesList')).click();
  }

  getParagraphText() {
    return element(by.css('.maik')).getText();
  }
}
