import { AppPage } from './app.po';

describe('exito App', () => {
  let page: AppPage;

  beforeEach(() => {
    page = new AppPage();
  });

  it('Buscando texto "Inicio"', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Inicio');
    page.openMenu().then(res => {
      console.log("Se ha desplegado el menú")
      page.openListItem();
    })
  });
});
