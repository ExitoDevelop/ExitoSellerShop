# **Éxito SellerShop Web**

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 1.6.1.

## Development server
Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding
Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build
Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `-prod` flag for a production build.

## Running unit tests
Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests
Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help
To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).

## **Información**

#### **Estructura básica de un componente**

- **example.component.scss**: Archivo de estilos en formatos scss (https://sass-lang.com/)
- example.component.ts
- example.component.html
- example.module.ts
- **example.component.spec.ts**: Se emplea para realizar las pruebas unitarias dentro de angular.
- **example.routing.ts**: Se emplea para definir la ruta de acceso que tendrá el componente.
- **example.shared.module.ts**:  Se emplea cuando el componente que estemos definiendo se piense implementar en uno o varios componentes externos, al definir un componente con **lazy load** no podemos incluirlo en el .module de nuestro componente, por lo que tenemos que crear un modulo aparte que se encargue de darnos acceso al componente que queremos y así poder utilizarlo en diferentes partes.

###### **ejemplo del archivo example.shared.module.ts**

```
import { ExampleComponent } from './sales-list.component';
import { ModuleWithProviders, NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const ExampleRoutes: Routes = [
  {
    path: '',
    component: ExampleComponent,
  }
];

/**
 * Creo el módulo que me permitirá importar la configuración de las rutas en el .module.ts.
 */
@NgModule({
  imports: [
    RouterModule.forChild(ExampleRoutes),
  ]
})

export class ExampleRouter { };

```

en el **.module.ts** solo importamos el router que creamos 
```
@NgModule({
    imports: [
        CommonModule,
        ``````` Otros imports ```
        ExampleRouter
    ]
})

export class ExampleModule { }
```

## **Firebase hosting**

> guía: https://medium.com/codingthesmartway-com-blog/hosting-angular-2-applications-on-firebase-f194688c978d


## **Pruebas Unitarias**
###### Preparando entorno.

* Guía de ng test: https://github.com/angular/angular-cli/wiki/test
* Guía de protractor: https://www.protractortest.org/#/tutorial
* Instalar **http-server**, esto permitirá ejecutar un servidor local donde visualizaremos el test de coverage donde veremos los resultados de las pruebas unitarias:   `npm install -g http-server`

###### Primeros pasos.
1. para iniciar la prueba unitaria correr el comando: `ng test  - ng test -sm=false` ó `ng test --code-coverage` para preparar el reporte de las pruebas, luego de ejecutar la primera prueba unitaria podemos visualizar el reporte de las pruebas con el comando `http-server coverage` esto lanzara un servidor donde visualizaremos el reporte de las pruebas.

## **Pruebas e2e con Angular**

Los archivos empleados para las pruebas e2e se encuentran en la carpeta **e2e** del proyecto. 

para ejecutar las pruebas se emplea el comando: `ng e2e`


#### **Pruebas e2e con Katalon**


1. Instalar python, instalar selenium module

`pip install selenium`

or depending on your permissions:

`sudo pip install selenium` ó `pip install -U selenium`

2. Instalar geckodriver. es un driver que se emplea para poder desplegar el navegador `brew install geckodriver`
3. Dar permisos para emplear el archivo geckodriver ubicado en la carpeta `usr/local/bin`: `sudo chmod 777 /usr/local/bin/geckodriver`

###### **Uso de katalon**

Se empleara python para ejecutar las pruebas e2e, python emplea un archivo con el formato `.py`  este archivo lo podemos exportar desde katalon y ejecutarlo por medio de la terminal. 

1. Exportar el archivo `.py` desde katalon. 
2. Ejecutar una sola prueba: `python file.py`
3. Ejecutar todos los archivos dentro de una carpeta:
 `for f in *.py; do python "$f"; done`


#### Correr el proyecto en los ambientes creados


1. `ng server --environment stage`
2. `ng server` --> default
3. `ng server --environment prod`