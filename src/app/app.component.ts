import { UserService } from './providers/services/user/user.service';
import { ComponentsService } from './providers/services/components/components.service';
import { EventEmitterOrders } from './providers/event/eventEmitter-orders.service';
import { MatSidenav } from '@angular/material';
import { Component, ViewChild, ViewEncapsulation, OnInit, EventEmitter } from '@angular/core';
import { Router, NavigationStart, NavigationEnd, NavigationError, NavigationCancel, RoutesRecognized } from '@angular/router';
import { User } from './providers/interface/user';

// Personal components


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  encapsulation: ViewEncapsulation.None,
  preserveWhitespaces: false
})
export class AppComponent implements OnInit {
  // Sidemenu
  @ViewChild('sidenav') sidenav: MatSidenav;
  @ViewChild('orderSidenav') orderSidenav: MatSidenav;

  public user: User;
  public title: string = "Éxito";
  public stateSideNav: Boolean = false;
  public stateSideNavOrder: Boolean = false;
  public loading: Boolean = false;



  constructor(private router: Router,
    public EventEmitterOrders: EventEmitterOrders,
    public COMPONENTS: ComponentsService,
    public USER: UserService) {
    this.router.events
      .filter(event => event instanceof NavigationStart)
      .subscribe((event: NavigationStart) => {
        // You only receive NavigationStart events
        this.getDataUser();
      });
  }

  /**
  * @whatItDoes Funcionalidades iniciales al arrancar la app.
  *
  * @stable
  */
  ngOnInit() {
    // funcion para validar la sesión del usuario
    this.getDataUser();
    this.valdiateRouterUser();
  }

  /**
   * @whatItDoes Funcionalidad encargada de traer la información del usuario que se encuentra almacenada en localstorage.
   *
   * @stable
   */
  getDataUser() {
    this.user = this.USER.getUser();
  }




  /**
  * @whatItDoes Funcionalidad encargada de validar el acceso del usaurio en la aplicación
  *
  * @stable
  */
  valdiateRouterUser() {

    if (this.user.login == true) {

      this.USER.getProfileUser(this.user.access_token).subscribe(res => {
        this.COMPONENTS.openSnackBar("has iniciado sesión con " + this.user.email, "Aceptar");
        // this.router.navigate(['/home']).then(res => {
        // this.stateSideNav = true;
        // });
      }, err => {
        this.COMPONENTS.openSnackBar("Se ha cerrado la sesión, Inicia de nuevo", "Aceptar");
        this.user = this.USER.getEmptyUser();
        this.USER.setUser(JSON.stringify(this.user));
        this.stateSideNav = false;
        this.router.navigate(['/seller-center-login']);
      })
    }
    else {
      this.user = this.USER.getEmptyUser();
      this.USER.setUser(JSON.stringify(this.user));
      this.stateSideNav = false;
      this.router.navigate(['/seller-center-login']);
    }
  }

  /**
  * @whatItDoes Funcionalidad que permite desplegar el menú.
  *
  * @stable
  */
  toggleMenu() {
    this.sidenav.toggle();
  }
}
