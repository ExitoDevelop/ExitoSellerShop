import { LoginConstants } from './providers/services/user/constants.service';
import { RecoveryComponent } from './pages/login/recovery-page/recovery.component';
import { ToolbarOptionsComponent } from './pages/orders/toolbar-options/toolbar-options.component';
import { OrdersListComponent } from './pages/orders/orders-list/orders-list.component';
import { HomeComponent } from './pages/home/home.component';
import { LoginComponent } from './pages/login/login-page/login.component';
import { SearchOrderMenuComponent } from './pages/orders/search-order-menu/search-order-menu.component';
import { LogoutComponent } from './pages/login/logout/logout.component';
import { ConfirmAlertComponent } from './providers/components/confirm-alert/confirm-alert.component';
import { MenuComponent } from './pages/menu/menu.component';
import 'hammerjs';

import { RouterModule } from '@angular/router';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';

// App components
import { AppComponent } from './app.component';
import { CdkDetailRowDirective } from './pages/orders/orders-list/cdk-detail-row.directive';
import { MaterialComponents } from './providers/components/material-components';
import { AppRoutingModule } from './providers/routing/root.service';
import { OrderService } from './providers/services/orders/orders.service';
import { GeneralConstants } from './providers/services/general-constants/general-constants.service';
import { UserService } from './providers/services/user/user.service';
import { EventEmitterOrders } from './providers/event/eventEmitter-orders.service';
import { ComponentsService } from './providers/services/components/components.service';
import { SendOrderComponent } from './pages/orders/send-order/send-order.component';
import { ProductsOrderComponent } from './pages/orders/orders-list/products-order/products-order.component';
import { ClientInformationComponent } from './pages/orders/orders-list/client-information/client-information.component';
import { OrderDetailModalComponent } from './pages/orders/orders-list/order-detail-modal/order-detail-modal.component';

@NgModule({
  declarations: [
    AppComponent,
    MenuComponent,
    ConfirmAlertComponent,
    LogoutComponent,
    SearchOrderMenuComponent,
    LoginComponent,
    HomeComponent,
    OrdersListComponent,
    ToolbarOptionsComponent,
    CdkDetailRowDirective,
    RecoveryComponent,
    SendOrderComponent,
    ProductsOrderComponent,
    ClientInformationComponent,
    OrderDetailModalComponent,
  ],
  imports: [
    BrowserModule,
    // Animaciones
    BrowserAnimationsModule,
    // Formularios
    ReactiveFormsModule,
    FormsModule,
    // Http
    HttpClientModule,
    // Material components
    MaterialComponents,
    // Routinh de la app
    AppRoutingModule,

  ],
  entryComponents: [SendOrderComponent, OrderDetailModalComponent],
  providers: [LogoutComponent, LoginConstants, OrderService, GeneralConstants, UserService, AppComponent, EventEmitterOrders, ComponentsService, OrdersListComponent],
  bootstrap: [AppComponent],

})
export class AppModule { }
