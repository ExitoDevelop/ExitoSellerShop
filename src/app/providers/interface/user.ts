/**
* @whatItDoes Estructura para el objeto que almacena el perfil del usuario.
*
*/
export interface User {
    login: boolean,
    nickname: string,
    name: string,
    role: number,
    last_name: string,
    email: string,
    email_verified: string,
    picture: string,
    access_token: string,
    sub: string,
    updated_at: string
}

/**
* @whatItDoes Estructura de el objeto enviado para iniciar sesión
*
*/
export interface Login {
    username: string,
    password: string
}
