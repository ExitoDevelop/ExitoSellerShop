
/**
 * @whatItDoes Estructura de los datos para la tabla.
 *
 * @stable
 */
export interface Order {
    Id: number;
    sendAllProduct: boolean;
    OrderNumber?: string;
    IdSeller: number;
    NameSeller?: string;
    NitSeller?: string;
    IdChannel: number;
    Channel?: string;
    DateOrder: string;
    IdStatusOrder: number;
    StatusOrder?: string;
    CostTotalOrder: number;
    CostTotalShipping: number;
    Commission: number;
    DateMaxDeliveryOrder: string;
    TypeDespatchOrder?: string;
    InfoClient: InfoClient;
    Products?: (ProductsEntity)[] | string;
}
export interface InfoClient {
    IdentificationCard?: string;
    NameClient?: string;
    Phone?: string;
    Address?: string;
}
export interface ProductsEntity {
    IdDetailProduct: number;
    IdPicking: number;
    NameProduct?: string;
    ProducId?: string;
    Sku?: string;
    Ean?: string;
    checkProductToSend: boolean;
    Reference?: string;
    IdStatusProduct: number;
    StatusProduct: number;
    Quantity: number;
    CostProduct: number;
    CostShippingProduct: number;
    Commission: number;
    DateMaxDeliveryProduct: string;
    TypeDespatchProduct?: string;
    Tracking?: string;
    Carrier?: string;
    DateDelivery: string;
}

export interface CategoryList {
    root: string,
    name: string,
    id: string,
    buttonId: string
}
