import { EventEmitter, Injectable } from '@angular/core';

@Injectable()
export class EventEmitterOrders {


    orderList = new EventEmitter<any>();
    filterOrderList = new EventEmitter<any>();


    /**
     * @whatItDoes Evento eventEmitter que permite crear un suscribe para saber cuando consultar las ordenes de acuerdo al estado proporcionado.
     *
     * @stable
     */
    getOrderList(state) {
        this.orderList.emit(state);
    }

    /**
     * @whatItDoes Evento eventEmitter que permite crear un suscribe para saber cuando listar una determinada lista de ordenes obtenidas por los filtros aplicados por el usuario en el componente search-order-menu
     *
     * @stable
     */
    filterOrderListResponse(data) {
        this.filterOrderList.emit(data)
    }

}
