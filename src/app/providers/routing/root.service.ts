import { OrdersListComponent } from './../../pages/orders/orders-list/orders-list.component';
import { HomeComponent } from './../../pages/home/home.component';
import { RecoveryComponent } from './../../pages/login/recovery-page/recovery.component';
import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';
import { LoginComponent } from '../../pages/login/login-page/login.component';

/**
  * Declaración de las rutas de la aplicación
  * ## Guía oficial
  * https://angular.io/guide/ngmodule#shared-modules
  * ## Ejemplo simple
  * ```
  * // Definiendo ruta con Lazi load.
  * { path: 'home', loadChildren: 'app/home/home.module#HomeModule' },
  * // Definiendo ruta sin lazy load, importando solo el componente.
  * { path: 'contact', component: ContactComponent}
  * ```
  */
const appRoutes: Routes =
  [
    { path: '', redirectTo: '/orders-list', pathMatch: 'full' },

    { path: 'seller-center-login', component: LoginComponent },

    { path: 'seller-center-recovery', component: RecoveryComponent },

    { path: 'home', component: HomeComponent },

    { path: 'orders-list', component: OrdersListComponent },
    { path: 'orders-list/status-orders/:category', component: OrdersListComponent },

  ];

/**
 * Creo el módulo que me permitira importar la configuración de las rutas en el app.module.
 */
@NgModule({
  imports: [RouterModule.forRoot(appRoutes, { enableTracing: true })],
  exports: [RouterModule]

})

export class AppRoutingModule { }
