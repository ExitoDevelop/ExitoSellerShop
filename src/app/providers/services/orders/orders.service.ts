import { environment } from './../../../../environments/environment';
import { GeneralConstants } from './../general-constants/general-constants.service';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class OrderService {



    constructor(private http: HttpClient, private GeneralConstants: GeneralConstants) { }



    getOrderList(state, token) {
        const headers = {
            headers: new HttpHeaders({
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${token}`

            })
        };
        if (state != undefined || state != null) {
            // return this.http.get(`${environment.apiUrl}orders`, data, headers);

            return this.http.get(`${environment.apiUrl}orders?IdStatusOrder=${state}`, headers);
        }
        else {
            return this.http.get(`${environment.apiUrl}orders`, headers);
        }
    }


    getOrdersFilter(filterInformation, token) {
        const headers = {
            headers: new HttpHeaders({
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${token}`

            })
        };
        // return this.http.get(`${environment.apiUrl}orders`, filterInformation, headers);

        return this.http.get(`${environment.apiUrl}orders`, headers);

    }

}
