import { environment } from './../../../../environments/environment';
import { LoginConstants } from './constants.service';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { User } from '../../interface/user';

const emptyuser: any = {
    login: false,
    nickname: false,
    name: '',
    role: 0,
    last_name: '',
    email: '',
    email_verified: '',
    picture: '',
    access_token: '',
    sub: '',
    updated_at: ''
};

@Injectable()
export class UserService {

    constructor(private http: HttpClient) { }

    getEmptyUser() {
        return emptyuser;
    }
    getUser() {
        let user: User = JSON.parse(localStorage.getItem("user"));
        return user || emptyuser;
    }
    setUser(data) {
        localStorage.setItem("user", data);
        let user: User = JSON.parse(localStorage.getItem("user"));
        return user || emptyuser;
    }

    loginUser(data) {
        const headers = {
            headers: new HttpHeaders({
                'Content-Type': 'application/json',
            })
        };
        // return this.http.post(`${ environment.auth0.url}oauth/token`, data, headers);
        return this.http.get(`${ environment.auth0.url}oauth/token`);

    }


    logout(token) {
        const headers = {
            headers: new HttpHeaders({
                'Content-Type': 'application/json',
                'Authorization': token
            })
        };
        return this.http.get(`${ environment.auth0.url}oauth/logout`, headers);
        // return this.http.get(`${ environment.auth0.url}logout`);
    }

    recoveryPassword(data) {
        const headers = {
            headers: new HttpHeaders({
                'Content-Type': 'application/json'
            })
        };
        // return this.http.get(`${ environment.auth0.url}recoverypassword`, data);
        return this.http.get(`${ environment.auth0.url}recoverypassword`);

    }


    getProfileUser(token) {
        console.log(token);
        const headers = {
            headers: new HttpHeaders({
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${token}`
            })
        };
        return this.http.get(`${ environment.auth0.url}userinfo`, headers);
        // return this.http.get(`${ environment.auth0.url}userinfo`, headers);

    }

}
