import { CategoryList, Order } from './../../interface/order';
import { Injectable } from '@angular/core';

@Injectable()
export class GeneralConstants {

    /**
     * @whatItDoes Variables empleadas en la lista de categorías
     *
     * @stable
     */


    public categoryList: Array<CategoryList> = [
        {
            root: "/orders-list",
            id: "",
            name: "Todas",
            buttonId: "allOrders"
        },
        {
            root: "/orders-list/status-orders/",
            name: "Por enviar",
            id: '1',
            buttonId: "for-send-orders"

        },
        {
            root: "/orders-list/status-orders/",
            name: "Enviar",
            id: '2',
            buttonId: "send-orders"

        },
        {
            root: "/orders-list/status-orders/",
            name: "En devolución",
            id: '3',
            buttonId: "devolucion-orders"

        },
        {
            root: "/orders-list/status-orders/",
            name: "Canceladas",
            id: '4',
            buttonId: "cancel-orders"

        }
    ];

    public emptyOrder: Order =
        {
            Id: 0,
            OrderNumber: "",
            sendAllProduct: false,
            IdSeller: 0,
            NameSeller: "",
            NitSeller: "",
            IdChannel: 0,
            Channel: "",
            DateOrder: "",
            IdStatusOrder: 0,
            StatusOrder: "",
            CostTotalOrder: 0,
            CostTotalShipping: 0,
            Commission: 0,
            DateMaxDeliveryOrder: "",
            TypeDespatchOrder: "",
            InfoClient: {
                IdentificationCard: "",
                NameClient: "",
                Phone: "",
                Address: ""
            },
            Products: [{
                IdDetailProduct: 0,
                IdPicking: 0,
                NameProduct: "",
                ProducId: "",
                Sku: "",
                Ean: "",
                checkProductToSend: false,
                Reference: "",
                IdStatusProduct: 0,
                StatusProduct: 0,
                Quantity: 0,
                CostProduct: 0,
                CostShippingProduct: 0,
                Commission: 0,
                DateMaxDeliveryProduct: "",
                TypeDespatchProduct: "",
                Tracking: "",
                Carrier: "",
                DateDelivery: ""
            }
            ]
        }


    constructor() { }


}
