import { MatSnackBar, MatDialog } from '@angular/material';
import { Injectable } from '@angular/core';
import { ConfirmAlertComponent } from '../../components/confirm-alert/confirm-alert.component';

@Injectable()
export class ComponentsService {

    constructor(public snackBar: MatSnackBar, public dialog: MatDialog) { }

    openSnackBar(message: string, action: string) {
        this.snackBar.open(message, action, {
            duration: 2000,
        });
    }
    openConfirmAlert(title: string, description?: string, ) {
        var promise = new Promise((resolve, reject) => {
            let dialogRef = this.dialog.open(ConfirmAlertComponent, {
                width: '360px',
                data: {
                    title: title,
                    description: description || null
                }
            })
            dialogRef.afterClosed().subscribe(result => {
                resolve(result);
            });
        })

        return promise;
    }
}
