import { NgModule, NO_ERRORS_SCHEMA, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import {
    MatButtonModule, MatExpansionModule, MatToolbarModule, MatIconModule, MatSlideToggleModule, MatCardModule, MatListModule, MatSelectModule, MatInputModule, MatGridListModule, MatTabsModule, MatFormFieldModule, MatSnackBarModule, MatSidenavModule, MatCheckboxModule, MatDialogModule, MatStepperModule, MatTooltipModule, MatNativeDateModule, MatProgressBarModule, MatMenuModule, MatSortModule, MatDatepickerModule, MatRippleModule, MatAutocompleteModule, MatButtonToggleModule, MatChipsModule, MatProgressSpinnerModule, MatRadioModule, MatSliderModule
} from '@angular/material';

import { MatTableModule } from '@angular/material/table';
import { MatPaginatorModule } from '@angular/material';
import { FlexLayoutModule } from '@angular/flex-layout';


@NgModule({
    imports: [
        // Angular Material
        MatButtonModule,
        MatMenuModule,
        MatTabsModule,
        MatToolbarModule,
        MatPaginatorModule,
        MatNativeDateModule,
        MatTableModule,
        MatDatepickerModule,
        MatPaginatorModule,
        MatIconModule,
        MatCardModule,
        MatSlideToggleModule,
        MatCheckboxModule,
        MatDialogModule,
        MatFormFieldModule,
        MatTableModule,
        MatExpansionModule,
        MatListModule,
        MatStepperModule,
        MatInputModule,
        MatGridListModule,
        MatProgressBarModule,
        MatSnackBarModule,
        MatSidenavModule,
        MatSelectModule,
        MatTooltipModule,
        MatSortModule,
        MatRippleModule,
        // Angular Material
        FlexLayoutModule,


        MatAutocompleteModule,
        MatButtonToggleModule,
        MatChipsModule,
        MatProgressSpinnerModule,
        MatRadioModule,
        MatSliderModule,

    ],
    exports: [
        // Angular Material
        MatButtonModule,
        MatMenuModule,
        MatTabsModule,
        MatToolbarModule,
        MatPaginatorModule,
        MatNativeDateModule,
        MatTableModule,
        MatPaginatorModule,
        MatIconModule,
        MatDatepickerModule,
        MatCardModule,
        MatSlideToggleModule,
        MatCheckboxModule,
        MatDialogModule,
        MatFormFieldModule,
        MatTableModule,
        MatExpansionModule,
        MatListModule,
        MatStepperModule,
        MatInputModule,
        MatGridListModule,
        MatProgressBarModule,
        MatSnackBarModule,
        MatSidenavModule,
        MatSelectModule,
        MatTooltipModule,
        MatSortModule,
        MatRippleModule,

        // Angular Material
        FlexLayoutModule,


        MatAutocompleteModule,
        MatButtonToggleModule,
        MatChipsModule,
        MatProgressSpinnerModule,
        MatRadioModule,
        MatSliderModule,

    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA]

})
export class MaterialComponents {
}
