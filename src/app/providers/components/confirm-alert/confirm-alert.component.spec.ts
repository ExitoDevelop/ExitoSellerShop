import { MatDialog } from '@angular/material/dialog';
import { OverlayContainer } from '@angular/cdk/overlay';
import { async, ComponentFixture, TestBed, inject } from '@angular/core/testing';

import { ConfirmAlertComponent } from './confirm-alert.component';
import { MatDialogRef, MatDialogModule } from '@angular/material';
import { MaterialComponents } from '../material-components';
import { BrowserDynamicTestingModule } from '@angular/platform-browser-dynamic/testing';

describe('ConfirmAlertComponent', () => {
  let component: ConfirmAlertComponent;
  let fixture: ComponentFixture<ConfirmAlertComponent>;
  let dialog: MatDialog;
  let overlayContainer: OverlayContainer;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ConfirmAlertComponent],
      imports: [MaterialComponents, MatDialogModule],
    })
    TestBed.overrideModule(BrowserDynamicTestingModule, {
      set: {
        entryComponents: [ConfirmAlertComponent]
      }
    });

    TestBed.compileComponents();
  }));

  beforeEach(inject([MatDialog, OverlayContainer],
    (d: MatDialog, oc: OverlayContainer) => {
      dialog = d;
      overlayContainer = oc;
    })
  );
});
