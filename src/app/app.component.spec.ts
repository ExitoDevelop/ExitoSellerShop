import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { OrderDetailModalComponent } from './pages/orders/orders-list/order-detail-modal/order-detail-modal.component';
import { ProductsOrderComponent } from './pages/orders/orders-list/products-order/products-order.component';
import { SendOrderComponent } from './pages/orders/send-order/send-order.component';
import { SearchOrderMenuComponent } from './pages/orders/search-order-menu/search-order-menu.component';
import { MenuComponent } from './pages/menu/menu.component';
import { APP_BASE_HREF } from '@angular/common';
import { TestBed, async } from '@angular/core/testing';
import { AppComponent } from './app.component';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule, CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA } from '@angular/core';
import { AppRoutingModule } from './providers/routing/root.service';
import { ConfirmAlertComponent } from './providers/components/confirm-alert/confirm-alert.component';
import { LogoutComponent } from './pages/login/logout/logout.component';
import { LoginComponent } from './pages/login/login-page/login.component';
import { HomeComponent } from './pages/home/home.component';
import { OrdersListComponent } from './pages/orders/orders-list/orders-list.component';
import { ToolbarOptionsComponent } from './pages/orders/toolbar-options/toolbar-options.component';
import { CdkDetailRowDirective } from './pages/orders/orders-list/cdk-detail-row.directive';
import { RecoveryComponent } from './pages/login/recovery-page/recovery.component';
import { ClientInformationComponent } from './pages/orders/orders-list/client-information/client-information.component';
import { HttpClientModule } from '@angular/common/http';
import { MaterialComponents } from './providers/components/material-components';
import { EventEmitterOrders } from './providers/event/eventEmitter-orders.service';
import { ComponentsService } from './providers/services/components/components.service';
import { UserService } from './providers/services/user/user.service';
import { HttpHandler, HttpClient } from '@angular/common/http';
import { OrderService } from './providers/services/orders/orders.service';
import { GeneralConstants } from './providers/services/general-constants/general-constants.service';

describe('AppComponent', () => {
  let component: AppComponent;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        AppComponent,
        MenuComponent,
        ConfirmAlertComponent,
        LogoutComponent,
        SearchOrderMenuComponent,
        LoginComponent,
        HomeComponent,
        OrdersListComponent,
        ToolbarOptionsComponent,
        CdkDetailRowDirective,
        RecoveryComponent,
        SendOrderComponent,
        ProductsOrderComponent,
        ClientInformationComponent,
        OrderDetailModalComponent,
      ],
      imports: [
        BrowserModule,
        // Animaciones
        BrowserAnimationsModule,
        // Formularios
        ReactiveFormsModule,
        FormsModule,
        // Http
        HttpClientModule,
        // Material components
        MaterialComponents,
        // Routinh de la app
        AppRoutingModule,

      ],
      providers: [{ provide: APP_BASE_HREF, useValue: '/' }, AppComponent, OrderService, EventEmitterOrders, ComponentsService, UserService, GeneralConstants, HttpClient, HttpHandler, LogoutComponent],
      schemas: [CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA]

    }).compileComponents();
  }));
  it('Creando la aplicación Éxito Angular', async(() => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app).toBeTruthy();

    it('Desplegando menu', () => {
      component.toggleMenu();
    });
  }));


  // it(`Verificando el titulo de la pagina'`, async(() => {
  //   const fixture = TestBed.createComponent(AppComponent);
  //   const app = fixture.debugElement.'componentInstance';
  //   expect(app.title).toEqual('Éxito');
  // }));
  // it('should render title in a h1 tag', async(() => {
  //   const fixture = TestBed.createComponent(AppComponent);
  //   fixture.detectChanges();
  //   const compiled = fixture.debugElement.nativeElement;
  //   expect(compiled.querySelector('h1').textContent).toContain('Welcome to app!');
  // }));
});
