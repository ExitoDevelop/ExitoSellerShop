import { OrderService } from './../../../providers/services/orders/orders.service';
import { UserService } from './../../../providers/services/user/user.service';
import { AppComponent } from './../../../app.component';
import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { User } from '../../../providers/interface/user';


@Component({
  selector: 'app-search-order-menu',
  templateUrl: './search-order-menu.component.html',
  styleUrls: ['./search-order-menu.component.scss'],
})
export class SearchOrderMenuComponent implements OnInit {

  selected = 'option1';
  myform: FormGroup;

  // user info
  public user: User;

  constructor(public AppComponent: AppComponent,
    public USER: UserService,
    private route: Router,
    private OrderService: OrderService,
    private fb: FormBuilder, ) { }

  ngOnInit() {
    // Obtengo la información del usuario
    this.user = this.USER.getUser();
    this.createForm();
  }

  createForm() {
    /**
       * @whatItDoes Estructura para los datos del formulario de consulta.
       *
       */
    this.myform = this.fb.group({
      'sinceDate': [null, Validators.compose([ ])],
      'untilDate': [null, Validators.compose([ ])],
      'ccClient': [null, Validators.compose([ ])],
      'typeOrder': [null, Validators.compose([ ])],
      'chanelOrder': [null, Validators.compose([ ])],
      'numberOrder': [null, Validators.compose([ , Validators.minLength(1), Validators.maxLength(30)])],
    });
  }

  clearForm() {
    this.myform.reset();
  }

  toggleMenu() {
    this.AppComponent.orderSidenav.toggle();
  }

  getOrderList(state) {
    this.AppComponent.EventEmitterOrders.getOrderList(state);
  }

  filterOrder(data) {
    console.log(data);
    this.route.navigate(['/orders-list']);
    this.OrderService.getOrdersFilter(data.value, this.user.access_token).subscribe((res: any) => {
      console.log(res);
      // indico a los elementos que esten suscriptos al evento.
      this.AppComponent.EventEmitterOrders.filterOrderListResponse(res);
      this.toggleMenu();
    })
  }
}
