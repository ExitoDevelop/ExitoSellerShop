import { EventEmitterOrders } from './../../../providers/event/eventEmitter-orders.service';
import { AppComponent } from './../../../app.component';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SearchOrderMenuComponent } from './search-order-menu.component';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MaterialComponents } from '../../../providers/components/material-components';
import { RouterTestingModule } from '@angular/router/testing';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ComponentsService } from '../../../providers/services/components/components.service';
import { UserService } from '../../../providers/services/user/user.service';
import { HttpClient, HttpHandler } from '@angular/common/http';
import { OrderService } from '../../../providers/services/orders/orders.service';
import { GeneralConstants } from '../../../providers/services/general-constants/general-constants.service';

describe('SearchOrderMenuComponent', () => {
  let component: SearchOrderMenuComponent;
  let fixture: ComponentFixture<SearchOrderMenuComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [SearchOrderMenuComponent],
      imports: [BrowserModule, FormsModule, MaterialComponents, RouterTestingModule, ReactiveFormsModule, BrowserAnimationsModule],
      providers: [AppComponent, EventEmitterOrders, ComponentsService, UserService, HttpClient, HttpHandler, OrderService, GeneralConstants]

    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SearchOrderMenuComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
