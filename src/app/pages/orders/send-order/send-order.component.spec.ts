import { MatDialog } from '@angular/material';
import { OverlayContainer } from '@angular/cdk/overlay';
import { EventEmitterOrders } from './../../../providers/event/eventEmitter-orders.service';
import { AppComponent } from './../../../app.component';
import { async, ComponentFixture, TestBed, inject } from '@angular/core/testing';

import { BrowserModule } from '@angular/platform-browser';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MaterialComponents } from '../../../providers/components/material-components';
import { RouterTestingModule } from '@angular/router/testing';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ComponentsService } from '../../../providers/services/components/components.service';
import { UserService } from '../../../providers/services/user/user.service';
import { HttpClient, HttpHandler } from '@angular/common/http';
import { OrderService } from '../../../providers/services/orders/orders.service';
import { GeneralConstants } from '../../../providers/services/general-constants/general-constants.service';
import { SendOrderComponent } from './send-order.component';
import { BrowserDynamicTestingModule } from '@angular/platform-browser-dynamic/testing';

describe('SendOrderComponent', () => {
  let component: SendOrderComponent;
  let fixture: ComponentFixture<SendOrderComponent>;
  let dialog: MatDialog;
  let overlayContainer: OverlayContainer;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [SendOrderComponent],
      imports: [BrowserModule, FormsModule, MaterialComponents, RouterTestingModule, ReactiveFormsModule, BrowserAnimationsModule],
      providers: [AppComponent, EventEmitterOrders, ComponentsService, UserService, HttpClient, HttpHandler, OrderService, GeneralConstants]
    })
    TestBed.overrideModule(BrowserDynamicTestingModule, {
      set: {
        entryComponents: [SendOrderComponent]
      }
    });

    TestBed.compileComponents();
  }));

  beforeEach(inject([MatDialog, OverlayContainer],
    (d: MatDialog, oc: OverlayContainer) => {
      dialog = d;
      overlayContainer = oc;
    })
  );


});
