import { Component, OnInit, Inject } from '@angular/core';
import { User } from '../../../providers/interface/user';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
// Load the full build.
import * as _ from "lodash";

@Component({
  selector: 'app-send-order',
  templateUrl: './send-order.component.html',
  styleUrls: ['./send-order.component.scss']
})
export class SendOrderComponent implements OnInit {

  user: User;
  order: any;

  constructor(public dialogRef: MatDialogRef<SendOrderComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any) {

    // _.cloneDeep permite clonar el json y no generar error de binding en la vista orders-list, ya que al usar el mimso json estaba presentando cambios en ambas vistas
    this.order = _.cloneDeep(data.order);
    this.user = data.user;

    console.log(this.order);
    console.log(this.user)
  }

  onNoClick(): void {
    this.dialogRef.close(false);
  }

  ngOnInit() {
  }


  // Funcionalidad para validar los check asociados a un productos.
  validateCheckProductForSendAll(item) {
    console.log("--validateCheckProductForSendAll")
    for (let j = 0; j < this.order.Products.length; j++) {
      // si un elemento check esta en false, desactivo el boton enviar todo.
      if (this.order.Products[j].checkProductToSend == false) {
        this.order.sendAllProduct = false;
        console.log("Hay uno o mas checks sin seleccionar, no se activa el boton enviar todo");
      }

    }
    // Luego de validar el estado false de los check, paso a validar el estado true para ver si el boton se puede activar o no.
    this.validateAllCheckProducts(item)
  }


  // Funcionalidad para validar si todos los checks de un prodcutos estan seleccionados
  validateAllCheckProducts(item) {

    // si la variable isAllChecked no cambia a false, es por que se puede activar el boton enviar todo.
    let isAllChecked = true;

    for (let j = 0; j < item.Products.length; j++) {
      if (item.Products[j].checkProductToSend != true) {
        isAllChecked = false;
      }

    }

    // si todos los check estan seleccionados, activo el boton enviar todo.
    if (isAllChecked) {
      this.order.sendAllProduct = true;
    }
  }


  // Funcionalidad para seleccionar todos los productos de una orden
  checkAllProductInOrder(item) {

    if (this.order.sendAllProduct == true) {
      this.order.sendAllProduct = false;
      for (let j = 0; j < this.order.Products.length; j++) {
        this.order.Products[j].checkProductToSend = false;
      }
    }
    else {
      this.order.sendAllProduct = true;

      for (let j = 0; j < this.order.Products.length; j++) {
        this.order.Products[j].checkProductToSend = true;
      }
    }
  }

  sendAllProductsOrder(item) {
    console.log("--sendAllProductsOrder");
    console.log(item);
  }

}
