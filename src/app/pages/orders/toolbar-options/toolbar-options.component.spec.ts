import { AppRoutingModule } from './../../../providers/routing/root.service';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ToolbarOptionsComponent } from './toolbar-options.component';
import { MaterialComponents } from '../../../providers/components/material-components';
import { Router } from '@angular/router';

describe('ToolbarOptionsComponent', () => {
  let component: ToolbarOptionsComponent;
  let fixture: ComponentFixture<ToolbarOptionsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ToolbarOptionsComponent],
      imports: [MaterialComponents],
      providers: [{ provide: Router, useClass: AppRoutingModule },]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ToolbarOptionsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
