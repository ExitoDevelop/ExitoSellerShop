import { GeneralConstants } from './../../../providers/services/general-constants/general-constants.service';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { CategoryList } from '../../../providers/interface/order';

@Component({
  selector: 'app-toolbar-options',
  templateUrl: './toolbar-options.component.html',
  styleUrls: ['./toolbar-options.component.scss'],
  providers: [GeneralConstants]

})
export class ToolbarOptionsComponent implements OnInit {

  public categoryList: any;
  constructor(private route: Router,
    private GeneralConstants: GeneralConstants
  ) {
    this.categoryList = this.GeneralConstants.categoryList;

  }

  ngOnInit() {
  }

  goToRoot(category: CategoryList) {
    if (category.id != '') {
      this.route.navigate([category.root, category.id]);
    }
    else {
      this.route.navigate([category.root]);
    }
  }

}
