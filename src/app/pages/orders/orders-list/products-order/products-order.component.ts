import { GeneralConstants } from './../../../../providers/services/general-constants/general-constants.service';
import { Component, OnInit } from '@angular/core';
import { Input } from '@angular/core';
import { Order } from '../../../../providers/interface/order';
import { MatTableDataSource } from '@angular/material';

@Component({
  selector: 'app-products-order',
  templateUrl: './products-order.component.html',
  styleUrls: ['./products-order.component.scss']
})
export class ProductsOrderComponent implements OnInit {

  constructor(public GeneralConstants: GeneralConstants) { }

  @Input() order: Order = this.GeneralConstants.emptyOrder;
  @Input() dataSource: any;



  ngOnInit() {

    console.log(this.order);
  }


  // Funcionalidad para validar los check asociados a un productos.
  validateCheckProductForSendAll(item) {
    console.log("--validateCheckProductForSendAll")
    // Encuentro en el objeto de la tabla actual la orden seleccionada
    for (let index = 0; index < this.dataSource.data.length; index++) {
      if (this.dataSource.data[index].OrderNumber == item.OrderNumber) {


        for (let j = 0; j < this.dataSource.data[index].Products.length; j++) {
          // si un elemento check esta en false, desactivo el boton enviar todo.
          if (this.dataSource.data[index].Products[j].checkProductToSend == false) {
            this.dataSource.data[index].sendAllProduct = false;
            console.log("Hay uno o mas checks sin seleccionar, no se activa el boton enviar todo");
          }

        }
        // Luego de validar el estado false de los check, paso a validar el estado true para ver si el boton se puede activar o no.
        this.validateAllCheckProducts(item)
      }
    }
  }


  // Funcionalidad para validar si todos los checks de un prodcutos estan seleccionados
  validateAllCheckProducts(item) {

    // si la variable isAllChecked no cambia a false, es por que se puede activar el boton enviar todo.
    let isAllChecked = true;

    for (let j = 0; j < item.Products.length; j++) {
      console.log(item.Products[j].checkProductToSend)
      if (item.Products[j].checkProductToSend != true) {
        isAllChecked = false;
      }

    }

    // si todos los check estan seleccionados, activo el boton enviar todo.
    if (isAllChecked) {
      for (let index = 0; index < this.dataSource.data.length; index++) {
        if (this.dataSource.data[index].OrderNumber == item.OrderNumber) {
          this.dataSource.data[index].sendAllProduct = true;
        }
      }
    }
  }


  // Funcionalidad para seleccionar todos los productos de una orden
  checkAllProductInOrder(item) {

    for (let index = 0; index < this.dataSource.data.length; index++) {
      if (this.dataSource.data[index].OrderNumber == item.OrderNumber) {
        if (this.dataSource.data[index].sendAllProduct == true) {
          for (let j = 0; j < this.dataSource.data[index].Products.length; j++) {
            this.dataSource.data[index].Products[j].checkProductToSend = false;
          }
          this.dataSource.data[index].sendAllProduct = false;
        }
        else {
          this.dataSource.data[index].sendAllProduct = true;

          for (let j = 0; j < this.dataSource.data[index].Products.length; j++) {
            this.dataSource.data[index].Products[j].checkProductToSend = true;
          }
        }
      }
    }
  }

  sendAllProductsOrder(item) {
    console.log("--sendAllProductsOrder");
    console.log(item);
  }


}
