import { GeneralConstants } from './../../../../providers/services/general-constants/general-constants.service';
import { MaterialComponents } from './../../../../providers/components/material-components';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProductsOrderComponent } from './products-order.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { RouterTestingModule } from '@angular/router/testing';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

describe('ProductsOrderComponent', () => {
  let component: ProductsOrderComponent;
  let fixture: ComponentFixture<ProductsOrderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ProductsOrderComponent],
      imports: [BrowserModule, FormsModule, MaterialComponents, RouterTestingModule, ReactiveFormsModule, BrowserAnimationsModule],
      providers: [GeneralConstants]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProductsOrderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
