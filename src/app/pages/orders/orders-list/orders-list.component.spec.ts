import { AppComponent } from './../../../app.component';
import { ProductsOrderComponent } from './products-order/products-order.component';
import { SendOrderComponent } from './../send-order/send-order.component';
import { OrderDetailModalComponent } from './order-detail-modal/order-detail-modal.component';

import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpClient, HttpHandler } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { RouterTestingModule } from '@angular/router/testing';
import { ReactiveFormsModule } from '@angular/forms';
import { MaterialComponents } from '../../../providers/components/material-components';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';

import { CommonModule } from '@angular/common';
import { OrdersListComponent } from './orders-list.component';
import { CdkDetailRowDirective } from './cdk-detail-row.directive';
import { ToolbarOptionsComponent } from '../toolbar-options/toolbar-options.component';
import { ClientInformationComponent } from './client-information/client-information.component';
import { EventEmitterOrders } from '../../../providers/event/eventEmitter-orders.service';
import { ComponentsService } from '../../../providers/services/components/components.service';
import { UserService } from '../../../providers/services/user/user.service';
import { LogoutComponent } from '../../login/logout/logout.component';
import { OrderService } from '../../../providers/services/orders/orders.service';
import { GeneralConstants } from '../../../providers/services/general-constants/general-constants.service';

describe('OrdersListComponent', () => {
  let component: OrdersListComponent;
  let fixture: ComponentFixture<OrdersListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [BrowserModule, FormsModule, MaterialComponents, RouterTestingModule, ReactiveFormsModule, BrowserAnimationsModule],
      declarations: [
        OrdersListComponent,
        CdkDetailRowDirective,
        OrdersListComponent,
        ToolbarOptionsComponent,
        OrderDetailModalComponent,
        SendOrderComponent,
        ClientInformationComponent,
        ProductsOrderComponent
      ],
      providers: [AppComponent, OrderService,GeneralConstants, EventEmitterOrders, ComponentsService, UserService, HttpClient, HttpHandler, LogoutComponent],


    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OrdersListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('Creando componente OrdersListComponent', () => {
    expect(component).toBeTruthy();
  });
});
