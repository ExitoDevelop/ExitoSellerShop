import { Order, CategoryList } from './../../../providers/interface/order';
import { OrderDetailModalComponent } from './order-detail-modal/order-detail-modal.component';
import { SendOrderComponent } from './../send-order/send-order.component';
import { UserService } from './../../../providers/services/user/user.service';
import { OrderService } from './../../../providers/services/orders/orders.service';
import { AppComponent } from './../../../app.component';
import { GeneralConstants } from './../../../providers/services/general-constants/general-constants.service';
import { Component, NgZone, OnInit, ViewChild, EventEmitter } from '@angular/core';
import { MatPaginator, MatTableDataSource, MatSort, MatSidenav, MatDialog } from '@angular/material';
import { SelectionModel } from '@angular/cdk/collections';
import { ActivatedRoute, Router } from '@angular/router';

// Módulo que permite personalizar los controles de la paginación de la tabla
import { MatPaginatorIntl } from '@angular/material';
import { animate, state, style, transition, trigger } from '@angular/animations';
import { User } from '../../../providers/interface/user';

@Component({
  selector: 'app-orders-list',
  templateUrl: './orders-list.component.html',
  styleUrls: ['./orders-list.component.scss'],
  // Configuración para la páginación de la tabla animations:
  animations: [
    trigger('detailExpand', [
      state('void', style({ height: '0px', minHeight: '0', visibility: 'hidden' })),
      state('*', style({ height: '*', visibility: 'visible' })),
      transition('void <=> *', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
    ]),
  ],
  providers: [GeneralConstants, { provide: MatPaginatorIntl, useValue: getDutchPaginatorIntl() }],

})

export class OrdersListComponent implements OnInit {

  // Elemento paginador
  @ViewChild(MatPaginator) paginator: MatPaginator;
  // Sort: elemento que se emplea para poder organizar los elementos de la tabla de acuerdo a la columna seleccionada
  @ViewChild(MatSort) sort: MatSort;



  // Columnas que se visualizan en la tabla
  public displayedColumns = ['select', 'plus', 'OrderNumber', 'DateOrder', 'DateMaxDeliveryOrder', 'StatusOrder', 'Channel', 'TypeDespatchOrder', 'detailOrder'];
  isExpansionDetailRow = (index, row) => row.hasOwnProperty('detailRow');
  public expandedElement: any;
  // Creo el elemento que se empleara para la tabla
  public dataSource: MatTableDataSource<Order>;
  public selection = new SelectionModel<Order>(true, [])
  // Variable que almacena el numero de elementos de la tabla
  public numberElements: number = 0;
  // estado por defecto para listar las ordenes
  public stateToListOrders: CategoryList;
  public currentCategory: any = {
    name: "Todas las ordenes",
    id: ""
  };
  public orderListLength: boolean = false;

  // suscriptions vars
  private subStateOrder: any;
  private subFilterOrder: any;
  private subOrderList: any;


  // user info
  public user: User;

  constructor(public AppComponent: AppComponent,
    private route: ActivatedRoute,
    private router: Router,
    public dialog: MatDialog,
    private GeneralConstants: GeneralConstants,
    private zone: NgZone,
    private OrderService: OrderService,
    public USER: UserService) {


  }

  ngOnInit() {
    console.log("Orders List component load");
    // Obtengo la información del usuario
    this.user = this.USER.getUser();
    /**
   * @whatItDoes Evento que permite escuchar cuando esten indicando cual tipo de orden consultar
   *
   * @stable
   */
    this.subOrderList = this.AppComponent.EventEmitterOrders.orderList.subscribe(
      (data: any) => {
        console.log("Consultando ordenes con el estado: " + data);
        this.getOrdersList(data);
      });

    /**
    * @whatItDoes Evento que permite obtener los parametros pasados por la url
    *
    * @stable
    */
    this.subStateOrder = this.route.params.subscribe(params => {
      let root = params['category'];
      console.log(root)
      if (root != undefined) {

        // logica para almacenar la categoria seleccionada
        let category = this.GeneralConstants.categoryList.filter(item => item.id == root);
        this.currentCategory = category[0];
        this.getOrdersList(root);
      }
      else {
        // obtengo las ordenes sin ningun estado en especifico
        this.getOrdersList();
      }
    });

    /**
    * @whatItDoes Evento que permite obtener los resultados obtenidos al momento de realizar el filtro de ordenes en la opcion search-order-menu
    *
    * @stable
    */
    this.subFilterOrder = this.AppComponent.EventEmitterOrders.filterOrderList.subscribe(
      (data: any) => {
        console.log("Aplicando resultados obtenidos por el filtro")
        if (data.length == 0) {
          this.orderListLength = true;
        }
        else {
          this.orderListLength = false;
        }
        this.dataSource = new MatTableDataSource(data);
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
        this.numberElements = this.dataSource.data.length;
      });
  }

  ngOnDestroy() {
    //Called once, before the instance is destroyed.
    //Add 'implements OnDestroy' to the class.
    this.subStateOrder.unsubscribe();
    this.subOrderList.unsubscribe();
    this.subFilterOrder.unsubscribe();
  }

  getAllOrderList() {
    this.router.navigate(['/orders-list']);
  }

  foo(event: Event) {
    event.stopPropagation();
  }
  /**
   * @whatItDoes Funcionalidad para consultar la lista de ordenes
   *
   * @stable
   */
  getOrdersList(state?) {
    this.OrderService.getOrderList(state, this.user.access_token).subscribe((res: any) => {
      this.addCheckOptionInProduct(res);
    }, err => {
      this.orderListLength = true;
      console.log(err)
    });
  }

  addCheckOptionInProduct(res) {
    console.log(res);
    // Logica para crear el contador de ordenes 
    if (res.length == 0) {
      this.orderListLength = true;
    }
    else {
      this.orderListLength = false;
    }
    // Logica para agregar la opción check en un producto
    for (let index = 0; index < res.length; index++) {

      // Agrego la opcion que habilitara el boton de enviar todo al seleccionar todos los checks
      res[index].sendAllProduct = false;
      for (let j = 0; j < res[index].Products.length; j++) {
        // agrego la opción de check a cada uno de los productos.
        res[index].Products[j].checkProductToSend = false;
      }
    }
    // Creo el elemento que permite pintar la tabla
    this.dataSource = new MatTableDataSource(res);
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
    this.numberElements = this.dataSource.data.length;
    console.log(this.dataSource)
  }


  /**
   * @whatItDoes Funcionalidad para despelgar el menu de filtro de ordenes.
   *
   * @stable
   */
  toggleMenuOrderSearch() {
    this.AppComponent.orderSidenav.toggle();
  }
  /**
   * Set the paginator after the view init since this component will
   * be able to query its view for the initialized paginator.
   */
  ngAfterViewInit() {
    // Instancia de el paginador y el sort de la tabla
    // this.dataSource.paginator = this.paginator;
    // this.dataSource.sort = this.sort;
  }

  // Filtro para la tabla
  applyFilter(filterValue: string) {
    filterValue = filterValue.trim(); // Remove whitespace
    filterValue = filterValue.toLowerCase(); // Datasource defaults to lowercase matches
    this.dataSource.filter = filterValue;
  }

  /** Whether the number of selected elements matches the total number of rows. */
  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataSource.data.length;
    return numSelected == numRows;
  }

  /** Selects all rows if they are not all selected; otherwise clear selection. */
  masterToggle() {
    this.isAllSelected() ?
      this.selection.clear() :
      this.dataSource.data.forEach(row => this.selection.select(row));
  }



  openDialogSendOrder(item): void {
    this.AppComponent.loading = true;
    let dialogRef = this.dialog.open(SendOrderComponent, {
      width: '95%',
      data: {
        user: this.user,
        order: item
      },
      panelClass: 'full-width-dialog'

    });
    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
      if (result == true) {
        this.AppComponent.loading = false;
        // this.getListElements();
      }
      else {
        this.AppComponent.loading = false;
      }
    });
  }

  openModalDetailOrder(item): void {
    let dialogRef = this.dialog.open(OrderDetailModalComponent, {
      // width: '90%',
      data: {
        user: this.user,
        order: item
      },
      // panelClass: 'full-width-dialog'
    });
    dialogRef.afterClosed().subscribe(result => {
      console.log('The modal detail order was closed');
      if (result == true) {
        // this.getListElements();
      }
      else {
      }
    });
  }
}


/**
 * @whatItDoes Configuración necesaria para personalizar la tabla.
 *
 * @stable
 */

export function getDutchPaginatorIntl() {
  const paginatorIntl = new MatPaginatorIntl();

  paginatorIntl.itemsPerPageLabel = 'página:';
  paginatorIntl.nextPageLabel = 'Siguiente página';
  paginatorIntl.previousPageLabel = 'Página previa';
  paginatorIntl.getRangeLabel = function (page, pageSize, length) {
    if (length === 0 || pageSize === 0) {
      return '0 de ' + length;
    }
    length = Math.max(length, 0);
    const startIndex = page * pageSize;
    // If the start index exceeds the list length, do not try and fix the end index to the end.
    const endIndex = startIndex < length ?
      Math.min(startIndex + pageSize, length) :
      startIndex + pageSize;
    return startIndex + 1 + ' - ' + endIndex + ' de ' + length;
  };;

  return paginatorIntl;
}





