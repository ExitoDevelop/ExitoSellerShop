import { GeneralConstants } from './../../../../providers/services/general-constants/general-constants.service';
import { Component, OnInit } from '@angular/core';
import { Order } from '../../../../providers/interface/order';
import { Input } from '@angular/core';

@Component({
  selector: 'app-client-information',
  templateUrl: './client-information.component.html',
  styleUrls: ['./client-information.component.scss']
})
export class ClientInformationComponent implements OnInit {

  @Input() order: Order = this.GeneralConstants.emptyOrder;

  constructor(public GeneralConstants: GeneralConstants) {

  }

  ngOnInit() {
  }

}
