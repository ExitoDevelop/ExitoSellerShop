
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ClientInformationComponent } from './client-information.component';
import { GeneralConstants } from '../../../../providers/services/general-constants/general-constants.service';

describe('ClientInformationComponent', () => {
  let component: ClientInformationComponent;
  let fixture: ComponentFixture<ClientInformationComponent>;
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ClientInformationComponent],
      providers: [GeneralConstants]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ClientInformationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
