import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { User } from '../../../../providers/interface/user';
// Load the full build.
import * as _ from "lodash";
import { Order } from '../../../../providers/interface/order';


@Component({
  selector: 'app-order-detail-modal',
  templateUrl: './order-detail-modal.component.html',
  styleUrls: ['./order-detail-modal.component.scss']
})
export class OrderDetailModalComponent implements OnInit {
  user: User;
  order: Order;

  constructor(public dialogRef: MatDialogRef<OrderDetailModalComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any) {

    // _.cloneDeep permite clonar el json y no generar error de binding en la vista orders-list, ya que al usar el mimso json estaba presentando cambios en ambas vistas
    this.order = _.cloneDeep(data.order);
    this.user = data.user;

    console.log(this.order);
    console.log(this.user)
  }



  onNoClick(): void {
    this.dialogRef.close(false);
  }
  ngOnInit() {
  }

}
