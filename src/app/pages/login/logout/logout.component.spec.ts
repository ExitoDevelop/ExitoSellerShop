import { ReactiveFormsModule } from '@angular/forms';
import { AppComponent } from './../../../app.component';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { EventEmitterOrders } from '../../../providers/event/eventEmitter-orders.service';
import { HttpClient, HttpHandler } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';


import { LogoutComponent } from './logout.component';
import { MaterialComponents } from '../../../providers/components/material-components';
import { ComponentsService } from '../../../providers/services/components/components.service';
import { UserService } from '../../../providers/services/user/user.service';
import { RouterTestingModule } from '@angular/router/testing';

describe('LogoutComponent', () => {
  let component: LogoutComponent;
  let fixture: ComponentFixture<LogoutComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [MaterialComponents, RouterTestingModule, ReactiveFormsModule, BrowserAnimationsModule],
      declarations: [LogoutComponent],
      providers: [AppComponent, EventEmitterOrders, ComponentsService, UserService, HttpClient, HttpHandler, LogoutComponent],

    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LogoutComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
