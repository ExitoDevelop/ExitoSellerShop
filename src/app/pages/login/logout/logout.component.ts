import { AppComponent } from './../../../app.component';
import { UserService } from './../../../providers/services/user/user.service';
import { ComponentsService } from './../../../providers/services/components/components.service';
import { Router, NavigationStart } from '@angular/router';
import { Component, OnInit, Input } from '@angular/core';
import 'rxjs/add/operator/filter';
import { User } from '../../../providers/interface/user';

@Component({
  selector: 'app-logout',
  templateUrl: './logout.component.html',
  styleUrls: ['./logout.component.css'],
  providers: [UserService, ComponentsService]
})
export class LogoutComponent implements OnInit {
  public user: any;
  @Input() action: string;

  constructor(public USER: UserService,
    public router: Router,
    public AppComponent: AppComponent,
    public COMPONENT: ComponentsService) {
    this.router.events
      .filter(event => event instanceof NavigationStart)
      .subscribe((event: NavigationStart) => {
        this.user = <User>this.USER.getUser();
      });
  }

  ngOnInit() {
    this.user = <User>this.USER.getUser();
  }

  logout() {
    this.AppComponent.loading = true;
    return new Promise(resolve => {
      this.USER.logout(this.user.access_token).subscribe((res: any) => {
        this.goToInitPage();
        this.COMPONENT.openSnackBar(res.message, "Aceptar");
        resolve(true);
      }, err => {
        this.goToInitPage();
        this.COMPONENT.openSnackBar("Se ha cerrado la sesión", "Aceptar");

      })
    })
  }

  goToInitPage() {
    this.user = this.USER.getEmptyUser()
    this.USER.setUser(JSON.stringify(this.user));
    this.AppComponent.stateSideNav = false;
    this.AppComponent.loading = false;
    this.router.navigate(['/seller-center-login']);
  }
}
