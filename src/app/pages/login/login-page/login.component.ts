import { environment } from './../../../../environments/environment';
import { Login, User } from './../../../providers/interface/user';
import { LoginConstants } from './../../../providers/services/user/constants.service';
import { ComponentsService } from './../../../providers/services/components/components.service';
import { Router } from '@angular/router';
import { Component, OnInit, trigger, state, style, animate, transition } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ViewContainerRef } from '@angular/core';
import { UserService } from '../../../providers/services/user/user.service';
import { AppComponent } from '../../../app.component';



@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
  providers: [
    UserService,
    LoginConstants],
  animations: [
    trigger('shrinkOut', [
      state('in', style({ opacity: 1, transform: 'translateX(0)' })),
      transition('void => *', [
        style({
          opacity: 0,
          transform: 'translateX(-100%)'
        }),
        animate('0.5s ease-in')
      ]),
      transition('* => void', [
        animate('0.5s 0.1s ease-out', style({
          opacity: 0,
          transform: 'translateX(100%)'
        }))
      ])
    ]),
    trigger('scaleEfect', [
      state('in', style({ opacity: 1, transform: 'translateX(0)' })),
      transition('void => *', [
        style({
          opacity: 0,
          transform: 'translateX(-100%)'
        }),
        animate('0.2s ease-in')
      ]),
      transition('* => void', [
        animate('0.2s 0.1s ease-out', style({
          opacity: 0,
          transform: 'translateX(100%)'
        }))
      ])
    ])
  ]

})
export class LoginComponent implements OnInit {

  myform: FormGroup;
  user: User;
  constructor(private fb: FormBuilder,
    public AppComponent: AppComponent,
    public router: Router,
    public LoginConstants: LoginConstants,
    public COMPONENT: ComponentsService,
    public USER: UserService,
  ) {
  }
  ngOnInit() {
    /**
    * @whatItDoes Estructura para los datos del formulario de login.
    *
    */
    this.myform = this.fb.group({
      'username': [null, Validators.required],
      'password': [null, Validators.compose([Validators.required, Validators.minLength(1), Validators.maxLength(30)])],
    });
    this.getDataUser();

    if (this.user.login == true) {
      this.router.navigate(['/orders-list']);
    }
    else {
      console.log("User not log")
    }
  }

  getDataUser() {
    this.user = this.USER.getUser();
  }

  /**
  * @whatItDoes Funcionalidad para realizar el proceso de login.
  * El flujo del proceso es:
  * 1. Obtengo los datos del formulario
  * 2. Armo el objeto json que se enviara al servicio de login
  * 3. De acuerdo a la respuesta muestro mensaje de error o paso a consultar la información del perfil del usuario
  *
  */
  loginUser(data) {

    // obtengo los datos del formulario de login
    let userInfo: Login = data.value;
    // armo el objeto que se enviara al servicio de login
    let information = {
      grant_type: "password",
      username: userInfo.username,
      password: userInfo.password,
      scope: "openid",
      client_id: environment.auth0.client_id,
      client_secret: environment.auth0.client_secret
    }

    // visualizo el componente loading.
    this.AppComponent.loading = true;

    this.USER.loginUser(information).subscribe((res: any) => {
      this.loginGetProfileUser(res);
    }, err => {
      this.viewErrorMessageLogin(err);
    });
  }


  /**
   * @whatItDoes Funcionalidad para obtener la información del usuario
   * El flujo del proceso es:
   * 1. Obtengo el token del usuario para permitir consultar el perfil
   * 2. De acuerdo a la respuesta muestro mensaje de inicio de sesión o paso a informar que el usuario no posee permisos para la vista.
   * 3. Redirigo al usuario a la vista principal de acuerdo a su rol
   */
  loginGetProfileUser(loginResponse) {
    console.log("Token info:");
    console.log(loginResponse);

    if (loginResponse.access_token != undefined) {

      this.USER.getProfileUser(loginResponse.access_token).subscribe((res: any) => {

        console.log("User profile");
        console.log(res);
        // Validación de permisos
        if (res != null) {
          // limpio formulario
          this.myform.reset();
          this.COMPONENT.openSnackBar("has iniciado sesión con " + res.name, "Aceptar");

          // agrego una variable al objeto del usuario para indicar que se encuentra logeado
          res.login = true;
          res.access_token = loginResponse.access_token;
          let user = JSON.stringify(res);
          this.USER.setUser(user);
          this.AppComponent.loading = false;
          this.router.navigate(['/orders-list']);
          // this.AppComponent.sidenav.toggle();
        } else {
          this.AppComponent.loading = false;
          this.COMPONENT.openSnackBar("Tu usuario no posee permiso para ingresar a este módulo", "Aceptar");
        }
      })
    }
    else {
      this.viewErrorMessageLogin();
    }
  }

  viewErrorMessageLogin(err?) {
    console.log(err)
    this.AppComponent.loading = false;

    this.COMPONENT.openSnackBar("Se ha presentado un error al iniciar sesión", "Aceptar");
  }


}
