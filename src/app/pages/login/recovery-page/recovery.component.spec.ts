import { HttpClient,HttpHandler } from '@angular/common/http';
import { AppComponent } from './../../../app.component';
import { ReactiveFormsModule } from '@angular/forms';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { MaterialComponents } from '../../../providers/components/material-components';
import { RouterTestingModule } from '@angular/router/testing';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { EventEmitterOrders } from '../../../providers/event/eventEmitter-orders.service';
import { ComponentsService } from '../../../providers/services/components/components.service';
import { UserService } from '../../../providers/services/user/user.service';
import { LogoutComponent } from '../logout/logout.component';

import { RecoveryComponent } from './recovery.component';


describe('RecoveryComponent', () => {
  let component: RecoveryComponent;
  let fixture: ComponentFixture<RecoveryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [MaterialComponents, RouterTestingModule, ReactiveFormsModule, BrowserAnimationsModule],
      providers: [AppComponent, EventEmitterOrders, ComponentsService, UserService, HttpClient, HttpHandler, LogoutComponent],
      declarations: [RecoveryComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RecoveryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
