import { ComponentsService } from './../../../providers/services/components/components.service';
import { AppComponent } from './../../../app.component';
import { UserService } from './../../../providers/services/user/user.service';
import { Router } from '@angular/router';
import { Component, OnInit, trigger, state, style, animate, transition, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormGroupDirective } from '@angular/forms';
import { ViewContainerRef } from '@angular/core';
import { FormControl } from '@angular/forms/src/model';

@Component({
  selector: 'app-recovery',
  templateUrl: './recovery.component.html',
  styleUrls: ['./recovery.component.scss'],
  providers: [
    UserService],
  animations: [
    trigger('shrinkOut', [
      state('in', style({ opacity: 1, transform: 'translateX(0)' })),
      transition('void => *', [
        style({
          opacity: 0,
          transform: 'translateX(-100%)'
        }),
        animate('0.5s ease-in')
      ]),
      transition('* => void', [
        animate('0.5s 0.1s ease-out', style({
          opacity: 0,
          transform: 'translateX(100%)'
        }))
      ])
    ]),
    trigger('scaleEfect', [
      state('in', style({ opacity: 1, transform: 'translateX(0)' })),
      transition('void => *', [
        style({
          opacity: 0,
          transform: 'translateX(-100%)'
        }),
        animate('0.2s ease-in')
      ]),
      transition('* => void', [
        animate('0.2s 0.1s ease-out', style({
          opacity: 0,
          transform: 'translateX(100%)'
        }))
      ])
    ])
  ]

})
export class RecoveryComponent implements OnInit {
  // @ViewChild(FormGroupDirective) formDirective: FormGroupDirective;
  @ViewChild('f') myNgForm;
  myform: FormGroup;

  constructor(private fb: FormBuilder,
    public AppComponent: AppComponent,
    public COMPONENT: ComponentsService,
    public USER: UserService,
    public appComponente: AppComponent,
  ) {
  }
  ngOnInit() {
    this.appComponente.loading = false;
    this.createForm()
  }

  createForm() {
    this.myform = this.fb.group({
      'email': ['', Validators.compose([Validators.required, Validators.email])],
    });
  }

  recoveryPassword(data) {
    let information = data.value;
    this.USER.recoveryPassword(information).subscribe((res: any) => {
      if (res != null) {
        // limpio formulario
        this.myform.setValue({
          email: '',
        });
        this.myform.reset();
        this.myNgForm.resetForm();
        this.AppComponent.loading = false;
        this.COMPONENT.openSnackBar(res.message, "Aceptar");
      }
      else {
        this.AppComponent.loading = false;
        this.COMPONENT.openSnackBar("Tu usuario no existe", "Aceptar");
      }
    }, err => {
      this.viewErrorMessageLogin(err);
    });
  }

  viewErrorMessageLogin(err?) {
    console.log(err)
    this.AppComponent.loading = false;
    if (err.error_description == "" || err.error_description == undefined) {
      err.error_description = "Se ha presentado un error al iniciar sesión";
    }
    this.COMPONENT.openSnackBar(err.error_description, "Aceptar");
  }

  // openSnackBar(message: string, action: string) {
  //   this.snackBar.open(message, action, {
  //     duration: 2000,
  //   });
  // }

}
