import { LogoutComponent } from './../login/logout/logout.component';
import { AppComponent } from './../../app.component';
import { GeneralConstants } from './../../providers/services/general-constants/general-constants.service';
import { Component, OnInit } from '@angular/core';
import 'rxjs/add/operator/filter';
import { MatSidenav } from '@angular/material';
import { Router } from '@angular/router';
import { CategoryList } from '../../providers/interface/order';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.scss'],
  providers: [GeneralConstants]
})
export class MenuComponent implements OnInit {

  public categoryList: any;
  constructor(public AppComponent: AppComponent,
    private GeneralConstants: GeneralConstants,
    private route: Router,
    public LogoutComponent: LogoutComponent) {

    this.categoryList = this.GeneralConstants.categoryList;
  }

  ngOnInit() { }

  toggleMenu() {
    this.AppComponent.sidenav.toggle();
  }
  logout() {
    this.LogoutComponent.logout();
  }
  getOrderList(state) {
    this.AppComponent.EventEmitterOrders.getOrderList(state);
  }
  goToRoot(category: CategoryList) {
    if (category.id != '') {
      this.route.navigate([category.root, category.id]);
    }
    else {
      this.route.navigate([category.root]);
    }
  }

}