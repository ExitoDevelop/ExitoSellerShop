import { HttpClient, HttpHeaders, HttpHandler } from '@angular/common/http';
import { AppComponent } from './../../app.component';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { MenuComponent } from './menu.component';
import { RouterTestingModule } from '@angular/router/testing';
import { MaterialComponents } from '../../providers/components/material-components';
import { EventEmitterOrders } from '../../providers/event/eventEmitter-orders.service';
import { ComponentsService } from '../../providers/services/components/components.service';
import { UserService } from '../../providers/services/user/user.service';
import { LogoutComponent } from '../login/logout/logout.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

describe('MenuComponent', () => {
  let component: MenuComponent;
  let fixture: ComponentFixture<MenuComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [MaterialComponents, RouterTestingModule, BrowserAnimationsModule],
      declarations: [MenuComponent],
      providers: [AppComponent, EventEmitterOrders, ComponentsService, UserService, HttpClient, HttpHandler, LogoutComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MenuComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('Creando componente MenuComponent', () => {
    expect(component).toBeTruthy();
  });

});
