// import { HomeComponent } from './home.component';
// import { ModuleWithProviders, NgModule } from '@angular/core';
// import { Routes, RouterModule } from '@angular/router';

// /**
//   * Declaración de las rutas de la aplicación
//   * ## Guía oficial
//   * https://angular.io/guide/ngmodule#shared-modules
//   * ## Ejemplo simple
//   * ```
//   * // Definiendo ruta con Lazi load.
//   * { path: 'home', loadChildren: 'app/home/home.module#HomeModule' },
//   * // Definiendo ruta sin lazy load, importando solo el componente.
//   * { path: 'contact', component: ContactComponent}
//   * ```
//   */
// const HomeRoutes: Routes = [
//   {
//     path: '',
//     component: HomeComponent,
//   }
// ];


// /**
//  * Creo el módulo que me permitira importar la configuración de las rutas en el app.module.
//  */
// @NgModule({
//   imports: [
//     RouterModule.forChild(HomeRoutes),
//   ],
  
// })

// export class HomeRouter { };
