import { AppComponent } from './../../app.component';

import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpClient, HttpHandler } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { RouterTestingModule } from '@angular/router/testing';
import { ReactiveFormsModule } from '@angular/forms';
import { MaterialComponents } from '../../providers/components/material-components';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';

import { EventEmitterOrders } from '../../providers/event/eventEmitter-orders.service';
import { ProductsOrderComponent } from './../orders/orders-list/products-order/products-order.component';
import { ClientInformationComponent } from './../orders/orders-list/client-information/client-information.component';
import { SendOrderComponent } from './../orders/send-order/send-order.component';
import { ToolbarOptionsComponent } from './../orders/toolbar-options/toolbar-options.component';
import { OrdersListComponent } from './../orders/orders-list/orders-list.component';
import { CommonModule } from '@angular/common';
import { HomeComponent } from './home.component';
import { OrderDetailModalComponent } from '../orders/orders-list/order-detail-modal/order-detail-modal.component';
import { CdkDetailRowDirective } from '../orders/orders-list/cdk-detail-row.directive';
import { ComponentsService } from '../../providers/services/components/components.service';
import { UserService } from '../../providers/services/user/user.service';
import { LogoutComponent } from '../login/logout/logout.component';
import { OrderService } from '../../providers/services/orders/orders.service';
import { GeneralConstants } from '../../providers/services/general-constants/general-constants.service';

describe('HomeComponent', () => {
  let component: HomeComponent;
  let fixture: ComponentFixture<HomeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [BrowserModule, FormsModule, MaterialComponents, RouterTestingModule, ReactiveFormsModule, BrowserAnimationsModule],
      declarations: [
        HomeComponent,
        CdkDetailRowDirective,
        OrdersListComponent,
        ToolbarOptionsComponent,
        OrderDetailModalComponent,
        SendOrderComponent,
        ClientInformationComponent,
        ProductsOrderComponent
      ],
      providers: [AppComponent, OrderService, GeneralConstants, EventEmitterOrders, ComponentsService, UserService, HttpClient, HttpHandler, LogoutComponent],


    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HomeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('Creando componente principal HomeComponent', () => {
    expect(component).toBeTruthy();
  });
});
